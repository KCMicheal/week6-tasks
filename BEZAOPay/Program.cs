﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEZAOPayDAL;
using BEZAOPayDAL.Models;


namespace BEZAOPay
{
    class Program
    {
        static void Main(string[] args)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["BezaoConnect"].ConnectionString;
            var db = new BEZAODAL();

            Console.WriteLine("**************Inserting a single user*************");

            User mUser = new User
            {
                Name = "Jos",
                Email = "jos@domain.com"
            };
            db.AddNewUser("js", "js@gmail.com");

            Console.WriteLine("**************Getting all users*************");

            var users = db.GetAllUsers();

            foreach (var user in users)
            {
                Console.WriteLine($"Id: {user.Id}\nName: {user.Name}\nEmail: {user.Email}");
            }

            Console.WriteLine("**************Getting a single user*************");

            var aUser = db.GetAUser(13);
            Console.WriteLine($"Id: {aUser.Id}\nName: {aUser.Name}\nEmail: {aUser.Email}");

            Console.WriteLine("**************Deleting a single user*************");
            db.DeleteUser(18);
            Console.WriteLine("The user has been deleted");

            /*Console.WriteLine("**************Adding account using parameter method*************");
            Account newAccount = new Account
            {
                UserId = 30,
                AccountNumber = 2010993949,
                Balance = 1293489
            };
            db.CreateAccount(newAccount);*/
            Console.WriteLine("**************Adding new account using stored procedures*************");
            db.AddNewAccount(25, 617204947, 4032845);
            Console.WriteLine("The new account has been added");
        }
    }
}
