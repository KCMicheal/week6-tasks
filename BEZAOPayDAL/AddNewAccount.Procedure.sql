﻿CREATE PROCEDURE AddNewAccount
	@UserId int,
	@Account_Number int,
	@Balance decimal(38,2)
AS
BEGIN
	INSERT INTO Accounts (UserId, Account_Number, Balance)
	VALUES (@UserId, @Account_Number, @Balance)
END