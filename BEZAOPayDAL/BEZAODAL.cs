﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using BEZAOPayDAL.Models;

namespace BEZAOPayDAL
{
   public class BEZAODAL
   {
       private readonly string _connectionString;

       public BEZAODAL():
           this(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=BEZAOPay;Integrated Security=True")
       {

       }
       
       public BEZAODAL(string connectionString )
       {
           _connectionString = connectionString;
       }


       private SqlConnection _sqlConnection = null;
       private void OpenConnection()
       {
           _sqlConnection = new SqlConnection { ConnectionString = _connectionString };
           _sqlConnection.Open();
       }

       private void CloseConnection()
       {
           if (_sqlConnection?.State != ConnectionState.Closed) 
               _sqlConnection?.Close();
       }


       public IEnumerable<User> GetAllUsers()
       {
            OpenConnection();

            var users = new List<User>();

            var query = @"SELECT * FROM USERS";

            using (var command = new SqlCommand(query, _sqlConnection))
            {
                command.CommandType = CommandType.Text;
                var reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                while (reader.Read())
                {
                   users.Add(new User
                   {
                       Id = (int) reader["Id"],
                       Name = (string) reader["Name"],
                       Email =  (string) reader["Email"]
                   }); 
                }
                reader.Close();
            }

            return users;
       }

        //Getting a single user
        public User GetAUser(int id)
        {
            OpenConnection();
            User aUser = null;
            string query = $"Select * From Users Where Id = {id}";
            using (var command = new SqlCommand(query, _sqlConnection))
            {
                command.CommandType = CommandType.Text;
                var dataReader = command.ExecuteReader(CommandBehavior.CloseConnection);
                while (dataReader.Read())
                {
                    aUser = new User
                    {
                        Id = (int)dataReader["Id"],
                        Name = (string)dataReader["Name"],
                        Email = (string)dataReader["Email"]
                    };
                }
                dataReader.Close();
            }
            return aUser;
        }

        //Add a new User
        public void AddNewUser(string userName, string email)
        {
            OpenConnection();
            string query = $"Insert Into Users (Name,  Email) Values ('{userName}', '{email}')";
            using (var command = new SqlCommand(query, _sqlConnection))
            {
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
            }
            CloseConnection();
        }

        //Adding a new user
        /*public void AddNewUser(User user)
        {
            OpenConnection();
            string query = $"Insert Into Users (Name,  Email) Values ('{user.Name}', '{user.Email}')";
            using (var command = new SqlCommand(query, _sqlConnection))
            {
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
            }
            CloseConnection();
        }*/

        //Delete a user
        public void DeleteUser(int Id)
        {
            OpenConnection();
            var query = $"Delete from Users Where Id = '{Id}'";
            using (var command = new SqlCommand(query, _sqlConnection))
            {
                try
                {
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Exception error = new Exception("Sorry! That User is admin!", ex);
                    throw;
                }
            }
            CloseConnection();
        }

        //Update a User
        public void UpdateUser(int Id, string name)
        {
            OpenConnection();
            var query = $"Update Users Set Name = '{name}' Where Id = '{Id}'";
            using(var command = new SqlCommand(query, _sqlConnection))
            {
                command.ExecuteNonQuery();
            }
            CloseConnection();
        }

        //Create Account using parameter method
        /*public void CreateAccount(Account acc)
        {
            OpenConnection();
            var query = "Insert Into Accounts (UserId, Account_Number, Balance) Values (@UserId, @Account_Number, @Balance)";
            using (var command = new SqlCommand(query, _sqlConnection))
            {
                SqlParameter parameter = new SqlParameter
                {
                    ParameterName = "@UserId",
                    Value = acc.UserId,
                    SqlDbType = SqlDbType.Int,
                    Size = 20
                };
                command.Parameters.Add(parameter);

                parameter = new SqlParameter
                {
                    ParameterName = "@Account_Number",
                    Value = acc.AccountNumber,
                    SqlDbType = SqlDbType.Int,
                    Size = 20
                };
                command.Parameters.Add(parameter);

                parameter = new SqlParameter
                {
                    ParameterName = "@Balance",
                    Value = acc.Balance,
                    SqlDbType = SqlDbType.Decimal,
                    Size = 20
                };
                command.Parameters.Add(parameter);

                command.ExecuteNonQuery();
                CloseConnection();
            }
        }*/

        //Stored Procedure method
        public void AddNewAccount(int userId, int accNum, decimal Bal)
        {
            OpenConnection();
            using (var command = new SqlCommand("AddNewAccount", _sqlConnection))
            {
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter parameter = new SqlParameter
                {
                    ParameterName = "@UserId",
                    SqlDbType = SqlDbType.Int,
                    Value = userId,
                    Direction = ParameterDirection.Input
                };
                command.Parameters.Add(parameter);

                parameter = new SqlParameter
                {
                    ParameterName = "@Account_Number",
                    Value = accNum,
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input
                };
                command.Parameters.Add(parameter);

                parameter = new SqlParameter
                {
                    ParameterName = "@Balance",
                    Value = Bal,
                    SqlDbType = SqlDbType.Decimal,
                    Direction = ParameterDirection.Input
                };
                command.Parameters.Add(parameter);

                command.ExecuteNonQuery();

                CloseConnection();
            }
        }
    }
}
